<htmlpageheader name="page-header">
	Bin Hire Quote from Invoice {{$invoiceDetails->paymentUniqueCode}}
</htmlpageheader>
	<!DOCTYPE html>
	<html>
		<head>
			 <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
			 <style >
				@media print { 
					body{
						font-size:11px;
						line-height:1.9px;
					}
					.table-responsive{
						margin:10px 0;
						line-height:1.7;
					}
					table{
						line-height:1.7;
					}
				} 
				
			</style>
		</head>
		<body>
			<div class="table-responsive">
				<table class="table table-condensed" border="0" width="100%" cellpadding="3">
					<tr style="text-align:center">
						<td width="50%">
							<img src="{{url('assets/images/skipbin-logo_03.png')}}" />
						</td>
						<td width="50%" style="padding-left: 20px;text-align:right">
							<address style="font-style:12px;">
								Ezyskips Online<br/>
								PO Box 14 Woodvale<br/>
								6026 <br/>
								0410 704 294<br/>
								info@ezyskipsonline.com.au<br/>
								ABN : 44 331 419 402
							</address>
						</td>
					</tr>
					<tr style="text-align:center">
						<td>
							<h3 style="text-transform:uppercase;">Tax Invoice no. {{$invoiceDetails->paymentUniqueCode}}</h3> <br />
							<strong ><?=date('l d-m-Y', strtotime($invoiceDetails->orderDate));?></strong ><br />
						</td>
					</tr>
				</table>
			</div>
			<div class="table-responsive">
				<table class="table table-condensed" border="0" cellpadding="3">
					<tr>
						<td >
							<h3 style="text-transform:uppercase;">Customer Delivery Details:</h3>
							
							<address style="font-style:12px;">
								{{$customerdetails->name}}<br/>
								{{$customerdetails->address}}<br/>
								{{$customerdetails->suburb}} <br/>
								{{$customerdetails->zipcode}} <br/>
								{{$customerdetails->phone}} <br/>
								{{$customerdetails->email}}
							</address>
						</td>
					</tr>
				</table>
			</div>
			
			<!--<div class="table-responsive">
				<table class="table table-condensed" border="0"  width="100%">
					<tr>
						<td >
							<h5>Delivery Address:</h5>
							<address>
								<?//=$invoiceDetails->deliveryAddress;?>
							</address>
						</td>
					</tr>
				</table>
			</div>-->
			<div class="table-responsive">
				<table class="table table-condensed" border="0"  width="100%" cellpadding="3">
					<tr>
						<td  width="50%">
							<h3 style="text-transform:uppercase;">Delivery Date:</h3>
							<address style="font-style:12px;">
								<?=date('l d-m-Y', strtotime($invoiceDetails->deliveryDate));?>
							</address>
						</td>
						<td  width="50%" style="padding-left: 20px">
							<h3 style="text-transform:uppercase">Collection Date:</h3>
							<address style="font-style:12px;">
								<?=date('l d-m-Y', strtotime($invoiceDetails->collectionDate));?>
							</address>
						</td>
					</tr>
				</table>
			</div>
			<div class="table-responsive">
				<table width="100%" class="table table-condensed" border="0" cellpadding="3">
					<tr>
						<td >
							<?php //if (isset($binhireoptions->extraHireagePrice) && ($binhireoptions->extraHireagePrice > 0) && ($binhireoptions->extraHireageDays > 0)) :?>
								<!--<p><span>Charged $<?//=$binhireoptions->extraHireagePrice?> after <?//=$binhireoptions->extraHireageDays?> days hire</span></p>-->
							<?php //endif;?>
							<?php if (isset($binhireoptions->excessWeightPrice) && ($binhireoptions->excessWeightPrice > 0)) :?>
								<p style="font-style:12px;"><span><strong>First 300kg included in price. Prorata price thereafter $<?=$binhireoptions->excessWeightPrice?> per 1000kg. </span></strong></p>
							<?php endif;?>
						</td>
					</tr>
				</table>
			</div>
			<div class="table-responsive">
				<table class="table table-condensed" border="1" style="border:0.5px solid #b1b2b2;" width="100%" cellpadding="3">
					<tbody>
						<tr style="background:#005343;text-align:center">
							<td colspan="3" style="color:#fff;text-align:center;text-transform:uppercase;"><strong><h3>Order summary</h3></strong></td>
						</tr>
						<tr>
							<td ><strong>Bin type</strong></td>
							<td colspan="2" ><p >{{$binhire->name}}</p></td>
						</tr>
						<tr>
							<td ><strong>Bin size</strong></td>
							<td colspan="2" ><p >{{$binhire->size}}</p></td>
						</tr>
						<tr>
							<td ><strong>Description</strong></td>
							<td colspan="2" >
								<?php $tags = array("strong", "b");?>
								<p ><?php echo preg_replace('#<(' . implode( '|', $tags) . ')(?:[^>]+)?>.*?</\1>#s', '', $binhire->description2);?></p><br />
								<p style="color:#c40005;"><?php echo $binhire->description; ?></p>
							</td>
						</tr>
						<tr>
							<td ><strong>Bin hire price</strong></td>
							<td colspan="2" ><p >${{sprintf('%1.2f',$invoiceDetails->subtotal)}}</p></td>
						</tr>
						<tr>
							<td ><strong>Booking fee</strong></td>
							<td colspan="2"><p >${{sprintf('%1.2f',$invoiceDetails->bookingfee)}}</p></td>
						</tr>
						<tr>
							<td ><strong>GST 10% paid</strong></td>
							<td colspan="2" ><p>${{sprintf('%1.2f',$invoiceDetails->gst)}}</p></td>
						</tr>
						<tr style="background:#005343;">
							<td style="color:#fff;"><strong>Grand total</strong></td>
							<td colspan="2" style="color:#fff;"><p >${{sprintf('%1.2f',$invoiceDetails->totalServiceCharge)}}</p></td>
						</tr>
						<tr style="text-align:center">
							<td colspan="3" style="text-align:center;text-transform:uppercase;" ><strong>Payment confirmed, due amount 0 (Zero).</strong></td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="table-responsive">
				<table class="table table-condensed" border="0" width="100%" cellpadding="3">
					<tr>
						<td width="50%" >
							<h3 style="text-transform:uppercase;">Supplier Details:</h3>
							<address  style="font-style:12px;">
								Business name : <i>{{$supplierdetails->name}}</i><br/>
								Contact name : <i>{{$supplierdetails->contactName}}</i><br/>
								Business hours phone number : <i>{{$supplierdetails->phonenumber}}</i><br/>
								Email: <i>{{$supplierdetails->email}}</i><br/>
							</address>
						</td>
					</tr>
				</table>
			</div>
		</body>
	</html>
<htmlpagefooter name="page-footer">
	{PAGENO}
</htmlpagefooter>