@extends('template')
@section('content')
		<div class="content-page">
			<div class="content">
				<div class="container-fluid">
					<div class="row">
						<div class="col-xl-12">
							<div class="breadcrumb-holder">
								<h1 class="main-title float-left">Suppliers' Fee</h1>
                                <ol class="breadcrumb float-right">
									<li class="breadcrumb-item"><a href="/instructions">Home</a></li>
                                    <li class="breadcrumb-item"><a href="{{ route('booking-fees.index') }}">All Suppliers' Fee</a></li>
									<li class="breadcrumb-item active">{{ $supplier->name }}</li>
								</ol>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-12">
							<div class="card mb-3 mt-2">
								<div class="card-header">
									Set Supplier Fee
								</div>

								<div class="card-body">
									<div class="row">
                                        <div class="col-md-4">
                                            <form method="post" action="{{ route('booking-fees.update', [ 'tblsupplier' => $supplier ]) }}">
                                                {{ csrf_field() }}

                                                <div class="form-group">
                                                    <label>Supplier</label>
                                                    <input type="text" readonly class="form-control-plaintext" value="{{ $supplier->name }}">
                                                </div>

												<div class="form-group">
                                                    <label>Username</label>
                                                    <input type="text" readonly class="form-control-plaintext" value="{{ $supplier->user->username }}">
                                                </div>

                                                <div class="form-group">
                                                    <label>Fee</label>
                                                    <div class="input-group">
                                                        <input type="text" name="amount" class="form-control" value="{{ old('amount', @$supplier->bookingFee->amount) }}">
                                                        <div class="input-group-prepend">
                                                            <select name="type" class="form-control">
                                                                <option value="percent" {{ old('amount', @$supplier->bookingFee->type) == 'percent' ? 'selected' : '' }}>% (percentage)</option>
                                                                <option value="fix" {{ old('amount', @$supplier->bookingFee->type) == 'fix' ? 'selected' : '' }}>AUD</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>

                                                <button type="submit" class="btn btn-primary">Submit</button>

                                            </form>
                                        </div>
                                    </div>
								</div>
							</div><!-- end card-->
						</div>
					</div>
				</div>
			</div>
		</div>
@endsection
