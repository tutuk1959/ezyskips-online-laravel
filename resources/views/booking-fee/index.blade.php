@extends('template')
@section('content')
		<div class="content-page">
			<div class="content">
				<div class="container-fluid">
					<div class="row">
						<div class="col-xl-12">
							<div class="breadcrumb-holder">
								<h1 class="main-title float-left">Suppliers' Fee</h1>
                                <ol class="breadcrumb float-right">
									<li class="breadcrumb-item"><a href="/instructions">Home</a></li>
									<li class="breadcrumb-item active">All Suppliers' Fee</li>
								</ol>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-12">
							@if (!empty(session('status')))
								@if (session('status') == 'danger')
									<div class="alert alert-danger" role="alert">
											{{session('message')}} <br />
									</div>
								@elseif (session('status') == 'success')
									<div class="alert alert-success" role="alert">
											{{session('message')}} <br />

									</div>
								@endif
							@endif

						</div>
					</div>
					<div class="row">
						<div class="col-12">
							<div class="card mb-3 mt-2">
								<div class="card-header">
									Active
								</div>

								<div class="card-body">
									@if (count($suppliers) > 0)
										<div class="alert alert-info" role="alert">
											The supplier list below only for approved suppliers. You need to approve supplier before set the fee.<br />
											To approve supplier, <a href="{{ route('user_management') }}">please go to this page</a>.
										</div>

										<div class="table-responsive">
											<table class="table table-bordered table-hover">
												<thead>
                                                    <th></th>
													<th>Supplier Name</th>
                                                    <th>User Name</th>
													<th>Fee</th>
													<th>Supplier Contact Name</th>
													<th>Supplier Email</th>
													<th>Supplier Phone Number</th>
												</thead>
												<tbody>
													@foreach($suppliers as $supplier)
														<tr>
                                                            <td>
                                                                <a class="btn btn-success btn-sm" href="{{ route('booking-fees.edit', [ 'tblsupplier' => $supplier ]) }}">Update</a>
															</td>
															<td>{{ $supplier->name }}</td>
                                                            <td>{{ $supplier->user->username }}</td>
															<td>
                                                                @if (!empty($supplier->bookingFee))
                                                                    {{ $supplier->bookingFee->amount }}
                                                                    {{ ($supplier->bookingFee->type == 'fix') ? 'AUD' : '%' }}
                                                                @else
                                                                    Not set
                                                                @endif
                                                            </td>
															<td>{{ $supplier->contactName }}</td>
															<td>{{ $supplier->email }}</td>
															<td>{{ $supplier->phonenumber }}</td>
														</tr>
													@endforeach
												</tbody>
											</table>
										</div>
									@else
										<p>No banned user list</p>
									@endif
								</div>
							</div><!-- end card-->
						</div>
					</div>
				</div>
			</div>
		</div>
@endsection
