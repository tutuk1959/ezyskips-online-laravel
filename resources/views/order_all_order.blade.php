@extends('template')
@section('content')
		<div class="content-page">
			<div class="content">
				<div class="container-fluid">
					<div class="row">
						<div class="col-xl-12">
							<div class="breadcrumb-holder">
								<h1 class="main-title float-left">All Orders</h1>
								<ol class="breadcrumb float-right">
									<li class="breadcrumb-item"><a href="/instructions">Home</a></li>
									<li class="breadcrumb-item active">All Orders Management</li>
								</ol>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-12">
							@if ($errors->has('selectsupplier'))
								<div class="alert alert-danger mr-2" role="alert">
									{{ $errors->first('selectsupplier') }}
								</div>
							@endif
							@if ($errors->has('selectstatus'))
								<div class="alert alert-danger mr-2" role="alert">
									{{ $errors->first('selectstatus') }}
								</div>
							@endif
							@if (!empty(session('status')))
								@if (session('status') == 'danger')
									<div class="alert alert-danger" role="alert">
											{{session('message')}} <br />
									</div>
								@elseif (session('status') == 'success')
									<div class="alert alert-success" role="alert">
											{{session('message')}} <br />
										
									</div>
								@endif
							@endif
						</div>
					</div>
					<div class="row">
						<div class="col-12">						
							<div class="card mb-3 mt-2">
								<div class="card-header">
									<strong>Order for All Suppliers (Based on Order Date)</strong>
								</div>
									
								<div class="card-body">
									<form action="/see_supplies_all_order_date" method="post" class="form-inline">
										{{csrf_field()}}
										<label class="mr-3" for="selectsupplier">Choose Date</label>
											<?php /*<input type="text" class="form-control mr-3" name="order_management_datepicker" width="30"> */ ?>
											<input type="text" class="form-control mr-3" name="fromdate" width="30">-
											<input type="text" class="form-control ml-3 mr-3" name="todate" width="30">
											<input type="hidden" name="start_order_date" value="">
											<input type="hidden" name="end_order_date" value="">
  										<button type="submit" class="btn btn-primary">See Supplies</button>
									</form>
								</div>
							</div><!-- end card-->
						</div>
					</div>
					
					@if(!is_null($suppliesdata))
					<div class="row">
						<div class="col-12">						
							<div class="card mb-3 mt-2">
								<div class="card-header">
									<strong>Order for All Suppliers (Based on Order Date)</strong>
								</div>
									
								<div class="card-body">
									<div class="table-responsive">
										<table class="table table-bordered" style="border:none;">
											<thead class="text-center">
												<th>Order Ref</th>
												<th>Order Date</th>
												<th>Customer Name</th>
												<!--<th>Customer Address</th>-->
												<th>Delivery Date</th>
												<th>Collection Date</th>
												<th>Price</th>
												<th>Order Status</th>
											
												<th>Change Status</th>
											</thead>
											<tbody>
												<?php $total = 0; $trans = 0;?>
												@foreach($suppliesdata as $data)
													<tr>
														<td><a href="{{ url('/') }}/order_detail/{{$data->paymentUniqueCode}}/{{$data->idSupplier}}/{{$data->idCustomer}}/{{$data->idBinType}}/{{$data->idBinService}}">{{$data->paymentUniqueCode}}</a></td>
														<td><p class="text-danger"><strong>{{date('d/m/Y', strtotime($data->orderDate))}}</strong></p></td>
														<td>{{$data->customerName}}</td>
														<!--<td>{{$data->deliveryAddress}}</td>-->
														<td>{{date('d/m/Y', strtotime($data->deliveryDate))}}</td>
														<td>{{date('d/m/Y', strtotime($data->collectionDate))}}</td>
														<td>${{$data->subtotal}}</td>	

														<td>
															@if(!is_null($data->status))
																@if($data->status == 1)
																	{{'Paid'}}
																@elseif($data->status == 2)
																	{{'Accepted'}}
																
																@endif
															@endif
														</td>
														
														<td>
															@if($data->status)
																<form action="/change_order_status" method="post">
																		{{csrf_field()}}
																		<input type="hidden" name="idOrder" value="{{$data->idOrderService}}">
																		<input type="hidden" name="idSupplier" value="{{$data->idSupplier}}">
																		<div class="form-row">
    																		<div class="col-12">
      																			<select name="selectstatus" id="selectstatus" class="form-control mr-3">
  																					<option value="#">Scroll down</option>
  																					<option value="2">Accepted</option>
  																					
  																				</select>
    																		</div>
    																		<div class="col-12 col-lg-4 m-1">
      																			<button type="submit" class="btn btn-primary mr-3">Update</button>
    																		</div>
  																		</div>
																</form>
															@endif
														</td>
													</tr>
													<?php $total = $total + $data->subtotal ; $trans = $trans + 1;?>
												@endforeach
												<tr >
													<td colspan="5" align="right"><strong class="text-danger">Total Transaction</strong></td>
													<td colspan="5"><strong>{{$trans}}</strong></td>
												</tr>
												<tr >
													<td colspan="5" align="right"><strong class="text-danger">Total Sales</strong></td>
													<td colspan="5"><strong>${{$total}}</strong></td>
												</tr>
											</tbody>
										</table>		
									</div>
								</div>
							</div><!-- end card-->
						</div>
					</div>
					@endif
				</div>
			</div>
		</div>
@endsection
