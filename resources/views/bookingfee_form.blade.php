@extends('template')
@section('content')
		<div class="content-page">
			<div class="content">
				<div class="container-fluid">
					<div class="row">
						<div class="col-xl-12">
							<div class="breadcrumb-holder">
								<h1 class="main-title float-left">Booking Fee Management</h1>
								<ol class="breadcrumb float-right">
									<li class="breadcrumb-item"><a href="/instructions">Home</a></li>
									<li class="breadcrumb-item active">Booking Fee</li>
								</ol>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-12">
							@if ($errors->has('price'))
								<div class="alert alert-danger mr-2" role="alert">
									{{ $errors->first('price') }}
								</div>
							@endif
						</div>
						<div class="col-12">
							<a href="{{ url('/') }}/booking_form/" class="mb-2 mr-2 ">
								<button name="button" type="button" class="btn btn-primary">
									<i class="fa fa-plus"></i> Add Booking Price
								</button>
							</a>
							<div class="card mb-3 mt-2">
								<div class="card-header">
									@if(!is_null($action))
										@if($action == 'edit')
											<strong>Edit Price List</strong>
										@else
											<strong>Add Price List</strong>
										@endif
									@else
										<strong>Add Price List</strong>
									@endif
									
								</div>
									
								<div class="card-body">
									@if(!is_null($action))
										@if($action == 'edit')
											@if(!is_null($bookingprice))
												<form action="/edit_bookingfee" method="post">
												{{csrf_field()}}
													<input type="hidden" value="{{$bookingprice->idUser}}" name="idUser">
													<input type="hidden" value="{{$bookingprice->idBookingPrice}}" name="idBookingFee">
													<div class="form-group">
														<label for="price">Price (in AUD)</label>
														<input id="price" type="text" class="form-control" name="price" value="{{$bookingprice->price}}" required=""/>
													</div>
													<button name="submit" type="submit" class="btn btn-primary">Save</button>
												</form>
											@endif
										@else
											<form action="/add_bookingfee" method="post">
												{{csrf_field()}}
												<div class="form-group">
													<label for="price">Price</label>
													<input id="price" type="text" class="form-control" name="price" required=""/>
												</div>
												<button name="submit" type="submit" class="btn btn-primary">Save</button>
											</form>
										@endif
									@else
										<form action="/add_bookingfee" method="post">
											{{csrf_field()}}
											<div class="form-group">
												<label for="price">Price</label>
												<input id="price" type="text" class="form-control" name="price" required=""/>
											</div>
											<button name="submit" type="submit" class="btn btn-primary">Save</button>
										</form>
									@endif
									
								</div>
							</div><!-- end card-->
						</div>
					</div>
				</div>
			</div>
		</div>
@endsection
