<?php
namespace App\Transformer;

use League\Fractal\TransformerAbstract;

class paymentTransformer extends TransformerAbstract {

    public function transform($id) {

    	$url = url('paymentdetails/');

        $paymentDetailUrl = route('addPayment', [ 'idpaymenttemp' => $id ]);

        return [
            'idpaymenttemp' => $id,
            'url' => $url,
            'payment_detail_url' => $paymentDetailUrl,
        ];
    }
 }
