<?php 
namespace App\Transformer;
 
use League\Fractal\TransformerAbstract;
 
class binhire_dailyTransformer extends TransformerAbstract {
 
    public function transform($binhire) {
        return [
            'idBinService' => $binhire->idBinService,
            'price' => $binhire->price,
            'stock' => $binhire->stock,
			'name' => $binhire->name,
			'size' => $binhire->size,
            'idSupplier' => $binhire->idSupplier,
            'idBinType' => $binhire->idBinType,
            'idBinSize' => $binhire->idBinSize,
        ];
    }
 }