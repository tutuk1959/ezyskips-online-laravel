<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tblpaymentformtemp extends Model
{
	protected $table = 'tblpaymentformtemp';
	public $timestamps = false;
}
