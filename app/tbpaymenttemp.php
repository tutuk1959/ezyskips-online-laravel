<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tblpaymenttemp extends Model
{
	protected $table = 'tblpaymenttemp';
	public $timestamps = false;
}
