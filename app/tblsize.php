<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tblsize extends Model
{
	protected $table = 'tblsize';
	public $timestamps = false;
}
