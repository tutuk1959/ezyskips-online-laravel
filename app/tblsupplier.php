<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tblsupplier extends Model
{
	protected $table = 'tblsupplier';

	protected $primaryKey = 'idSupplier';

	public $timestamps = false;

	/**
	* One to one relationship with BookingFeeSetting
	*/
	public function bookingFee()
	{
		return $this->hasOne(BookingFeeSetting::class, 'supplier_id', $this->primaryKey);
	}

	/**
	* One to one relationship with
	*/
	public function user()
	{
		return $this->belongsTo(tbluse::class, 'idSupplier', 'idSupplier');
	}

	/**
	* Define scope based on approved user
	*/
	public function scopeApproved($query)
	{
		return $query->whereHas('user', function ($query) {
			return $query->where([
				'isActive'      => 1,
				'role'          => 2,
				'adminApproved' => 1,
				'userStatus'    => 1,
			]);
		});
	}
}
