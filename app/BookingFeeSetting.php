<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BookingFeeSetting extends Model
{
    protected $fillable = [
        'supplier_id',
        'type',
        'amount'
    ];

    public function getAmountTextAttribute()
    {
        return ($this->type == 'fix') ? "${$this->amount}" : "{$this->amount}%";
    }
}
