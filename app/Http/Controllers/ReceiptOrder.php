<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\tblcustomer;
use App\tblorderservice;
use App\tblbintype;
use App\tblsize;
use App\tblbinservice;
use App\tblbinserviceoptions;
use App\tblbinserviceupdates;
use App\tbluse;
use App\supplier;
use App\tblbinnondelivery;
use App\tblorderstatus;
use App\tblsupplier;
use Illuminate\Support\Facades\DB;
use PDF;
use Redirect;
use Validator;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;

/**
* Backend - Order receipt
**/
class ReceiptOrder extends Controller
{
	public function index(){
		$idUser = 	session('idUser');
		$supplierData = $this->getSupplierData($idUser);
		$orders = $this->getOrdersData($supplierData->idSupplier);

		return view('order_receipt', [ 'supplierData' => $supplierData, 'orders' => $orders, 'errorStatus' => 'false'
		]);
	}

	private function get_customer_detail($idCustomer){
		$customerdetails = DB::table('tblcustomer')
        		->select('idCustomer', 'name', 'address', 'email', 'phone', 'suburb', 'zipcode')
        		->where(['idCustomer' => $idCustomer])
        		->first();
        return $customerdetails;
	}

	private function getSupplierData($idUser){
		$supplierData = DB::table('tbluser')
			->leftJoin('tblsupplier','tblsupplier.idSupplier', '=', 'tbluser.idSupplier')
			->select('tblsupplier.idSupplier', 'tblsupplier.name', 'tblsupplier.contactName','tblsupplier.email', 'tblsupplier.email2', 'tblsupplier.phonenumber', 'tblsupplier.email2',
				'tblsupplier.mobilePhone', 'tblsupplier.isOpenSaturday', 'tblsupplier.isOpenSunday', 'tblsupplier.customerServiceContact',
				'tblsupplier.customerServicePhone','tblsupplier.customerServiceMobile', 'tblsupplier.fullAddress', 'tblsupplier.abn',
				'tbluser.idUser', 'tbluser.idSupplier','tbluser.username', 'tbluser.password', 'tbluser.isActive', 'tbluser.role')
			->where('tbluser.idUser', $idUser)
			->groupBy('tblsupplier.idSupplier','tblsupplier.name', 'tblsupplier.contactName','tblsupplier.email', 'tblsupplier.email2', 'tblsupplier.phonenumber', 'tblsupplier.email2',
				'tblsupplier.mobilePhone', 'tblsupplier.isOpenSaturday', 'tblsupplier.isOpenSunday', 'tblsupplier.customerServiceContact',
				'tblsupplier.customerServicePhone','tblsupplier.customerServiceMobile', 'tblsupplier.fullAddress', 'tblsupplier.abn',
				'tbluser.idUser', 'tbluser.idSupplier','tbluser.username', 'tbluser.password', 'tbluser.isActive', 'tbluser.role')
			->first();
		return $supplierData;
	}

	private function get_supplier_data_by_id($idSupplier){
		$supplierdata = DB::table('tbluser')
			->leftJoin('tblsupplier','tblsupplier.idSupplier', '=', 'tbluser.idSupplier')
			->select('tblsupplier.idSupplier', 'tblsupplier.name', 'tblsupplier.contactName','tblsupplier.email', 'tblsupplier.email2', 'tblsupplier.phonenumber', 'tblsupplier.email2',
				'tblsupplier.mobilePhone', 'tblsupplier.isOpenSaturday', 'tblsupplier.isOpenSunday', 'tblsupplier.customerServiceContact',
				'tblsupplier.customerServicePhone','tblsupplier.customerServiceMobile', 'tblsupplier.fullAddress',
				'tbluser.idUser', 'tbluser.idSupplier','tbluser.username', 'tbluser.password', 'tbluser.isActive', 'tbluser.role')
			->where([
				'tblsupplier.idSupplier' => $idSupplier
			])
			->groupBy('tblsupplier.idSupplier','tblsupplier.name', 'tblsupplier.contactName','tblsupplier.email', 'tblsupplier.email2', 'tblsupplier.phonenumber', 'tblsupplier.email2',
				'tblsupplier.mobilePhone', 'tblsupplier.isOpenSaturday', 'tblsupplier.isOpenSunday', 'tblsupplier.customerServiceContact',
				'tblsupplier.customerServicePhone','tblsupplier.customerServiceMobile', 'tblsupplier.fullAddress',
				'tbluser.idUser', 'tbluser.idSupplier','tbluser.username', 'tbluser.password', 'tbluser.isActive', 'tbluser.role')
			->first();
		return $supplierdata;
	}

	private function get_bin_service_options($idSupplier, $idBinType){
		$binhireoptions = DB::table('tblbinserviceoptions')
                ->leftJoin('tblbinservice', function ($query){
							$query->on('tblbinservice.idBinType', '=', 'tblbinserviceoptions.idBinType')
									->on('tblbinservice.idSupplier', '=', 'tblbinserviceoptions.idSupplier');
							})
				->select('tblbinserviceoptions.idBinServiceOptions', 'tblbinserviceoptions.extraHireagePrice', 'tblbinserviceoptions.extraHireageDays', 'tblbinserviceoptions.excessWeightPrice')
				->where([
					'tblbinserviceoptions.idBinType' => $idBinType,
					'tblbinserviceoptions.idSupplier' =>$idSupplier
					])
				->orderBy('tblbinserviceoptions.idBinServiceOptions', 'tblbinserviceoptions.extraHireagePrice', 'tblbinserviceoptions.extraHireageDays', 'tblbinserviceoptions.excessWeightPrice')
				->first();
		return $binhireoptions;
	}

	private function get_bin_hire($idBinHire){
		$binhire = DB::table('tblbinservice')
			->leftJoin('tblsize', 'tblsize.idSize', '=','tblbinservice.idBinSize')
			->leftJoin('tblbintype','tblbintype.idBinType','=','tblbinservice.idBinType')
			->leftJoin('tblbinnondelivery', 'tblbinservice.idBinType','=', 'tblbinnondelivery.idBinType')
			->leftJoin('tblbinserviceupdates','tblbinservice.idBinService','=','tblbinserviceupdates.idBinService')
			->leftJoin('tblbinserviceoptions', 'tblbinservice.idBinType','=', 'tblbinserviceoptions.idBinType')
			->leftJoin('tbluserservicearea', 'tblbinservice.idSupplier', '=', 'tbluserservicearea.idSupplier')
			->leftJoin('tblservicearea', 'tblservicearea.idArea','=','tbluserservicearea.idServiceArea')
			->select('tblbinservice.price', 'tblbinservice.stock', 'tblbinservice.idBinService', 'tblbinservice.idBinType',
                                        'tblbinservice.idBinSize','tblbinservice.idSupplier', 'tblbintype.name', 'tblbintype.description', 'tblbintype.description2','tblsize.size')
			->where(['tblbinservice.idBinService' => $idBinHire])
			->groupBy('tblbinservice.price', 'tblbinservice.stock', 'tblbinservice.idBinService', 'tblbinservice.idBinType',
								'tblbinservice.idBinSize','tblbinservice.idSupplier', 'tblbintype.name', 'tblbintype.description', 'tblbintype.description2','tblsize.size')
            ->orderBy('tblbinservice.price','asc')
            ->first();

         return $binhire;
	}

	private function get_order_detail($orderref, $idSupplier){
		$invoiceDetails = DB::table('tblorderservice')
        				->select('idSupplier','idBinService','idConsumer','paymentUniqueCode', 'totalServiceCharge',
						'gst', 'subtotal', 'bookingfee',
						'deliveryDate', 'collectionDate', 'deliveryAddress', 'deliveryComments', 'orderDate','card_category','card_type','card_number','card_holder')
        				->where(['paymentUniqueCode' => $orderref, 'idSupplier' => $idSupplier])
        				->first();
		 return $invoiceDetails;
	}


	private function getOrdersData($idSupplier){
		$orders = DB::table('tblorderservice')
			->leftJoin('tblcustomer','tblorderservice.idConsumer', '=', 'tblcustomer.idCustomer')
			->leftJoin('tblsupplier', 'tblorderservice.idSupplier','=', 'tblsupplier.idSupplier')
			->leftJoin('tbluser', 'tbluser.idSupplier', '=', 'tblsupplier.idSupplier')
			->leftJoin('tblbinservice','tblbinservice.idBinService','=','tblorderservice.idBinService')
			->leftJoin('tblbintype','tblbinservice.idBinType','=','tblbintype.idBinType')
			->leftJoin('tblsize','tblbinservice.idBinSize','=','tblsize.idSize')
			->leftJoin('tblorderstatus', 'tblorderstatus.idOrder', '=', 'tblorderservice.idOrderService')
			->select('tblcustomer.name AS customerName','tblcustomer.company AS customerCompanyName', 'tblcustomer.email AS customerEmail','tblcustomer.phone AS customerPhone',
				'tblbintype.name AS bintypename','tblbintype.CodeType as bintypecode','tblbintype.description AS bintypedescription',
				'tblsize.size AS binsize', 'tblorderservice.idOrderService','tblorderservice.paymentUniqueCode',
				'tblorderservice.orderDate', 'tblorderservice.deliveryDate', 'tblorderservice.collectionDate', 'tblorderservice.totalServiceCharge', 'tblorderservice.deliveryAddress',
				'tblorderstatus.status', 'tblorderstatus.created_at', 'tblorderstatus.updated_at', 'tblorderstatus.operator', 'tblsupplier.idSupplier', 'tblcustomer.idCustomer',
				'tblbintype.idBinType', 'tblorderservice.idBinService', 'tblorderservice.subtotal'
				)
			->where([
				'tblsupplier.idSupplier' => $idSupplier
			])
			->orderBy('tblorderservice.orderDate', 'DESC')
			->groupBy('customerName','customerCompanyName','customerEmail','customerPhone','bintypename','bintypecode','bintypedescription',
				'binsize','tblorderservice.idOrderService','tblorderservice.paymentUniqueCode','tblorderservice.orderDate', 'tblorderservice.deliveryDate',
				'tblorderservice.collectionDate', 'tblorderservice.totalServiceCharge', 'tblorderservice.deliveryAddress',
				'tblorderstatus.status', 'tblorderstatus.created_at', 'tblorderstatus.updated_at', 'tblorderstatus.operator', 'tblsupplier.idSupplier', 'tblcustomer.idCustomer',
				'tblbintype.idBinType', 'tblorderservice.idBinService', 'tblorderservice.subtotal'
				)
			->get();
		return $orders;
	}

	public function orderservicedetail(Request $request){
		$idorderservice = $request['idorderservice'];
		$invoiceDetails = DB::table('tblorderservice')
        				->select('idSupplier','idBinService','idConsumer','paymentUniqueCode',
						'totalServiceCharge', 'subtotal','gst', 'bookingfee',
						'deliveryDate', 'collectionDate', 'deliveryAddress', 'deliveryComments', 'orderDate','card_category','card_type','card_number','card_holder')
        				->where(['idOrderService' => $idorderservice])
        				->first();
        if (is_null($invoiceDetails)){
        	return view('order_receipt', [ 'errorStatus' => 'invoice_not_found']);
        } else {
        	$binhire = DB::table('tblbinservice')
			->leftJoin('tblsize', 'tblsize.idSize', '=','tblbinservice.idBinSize')
			->leftJoin('tblbintype','tblbintype.idBinType','=','tblbinservice.idBinType')
			->leftJoin('tblbinnondelivery', 'tblbinservice.idBinType','=', 'tblbinnondelivery.idBinType')
			->leftJoin('tblbinserviceupdates','tblbinservice.idBinService','=','tblbinserviceupdates.idBinService')
			->leftJoin('tblbinserviceoptions', 'tblbinservice.idBinType','=', 'tblbinserviceoptions.idBinType')
			->leftJoin('tbluserservicearea', 'tblbinservice.idSupplier', '=', 'tbluserservicearea.idSupplier')
			->leftJoin('tblservicearea', 'tblservicearea.idArea','=','tbluserservicearea.idServiceArea')
			->select('tblbinservice.price', 'tblbinservice.stock', 'tblbinservice.idBinService', 'tblbinservice.idBinType',
                                        'tblbinservice.idBinSize','tblbinservice.idSupplier', 'tblbintype.name', 'tblbintype.description', 'tblbintype.description2','tblsize.size')
			->where(['tblbinservice.idBinService' => $invoiceDetails->idBinService])
			->groupBy('tblbinservice.price', 'tblbinservice.stock', 'tblbinservice.idBinService', 'tblbinservice.idBinType',
								'tblbinservice.idBinSize','tblbinservice.idSupplier', 'tblbintype.name', 'tblbintype.description','tblbintype.description2', 'tblsize.size')
            ->orderBy('tblbinservice.price','asc')
            ->first();

            $customerdetails = DB::table('tblcustomer')
        		->select('idCustomer', 'name', 'address', 'email', 'phone', 'suburb', 'zipcode')
        		->where(['idCustomer' => $invoiceDetails->idConsumer])
        		->first();

        	$supplierdetails = DB::table('tblsupplier')
            					->select('name', 'contactName', 'phonenumber', 'email', 'mobilePhone', 'fullAddress', 'email2',
                                                    'customerServiceContact', 'customerServicePhone', 'customerServiceMobile','abn')
            					->where(['idSupplier' => $invoiceDetails->idSupplier])
            					->first();

			$deliveryYear = date('Y', strtotime($invoiceDetails->deliveryDate));
			$deliveryMonth = date('m', strtotime($invoiceDetails->deliveryDate));
			$deliveryDay = date('d', strtotime($invoiceDetails->deliveryDate));

			$collectionYear = date('Y', strtotime($invoiceDetails->collectionDate));
			$collectionMonth = date('m', strtotime($invoiceDetails->collectionDate));
			$collectionDay = date('d', strtotime($invoiceDetails->collectionDate));

			$startdate = Carbon::createFromDate($deliveryYear, $deliveryMonth, $deliveryDay);
			$finishdate = Carbon::createFromDate($collectionYear, $collectionMonth, $collectionDay);
			$datemargin = $startdate->diffInDays($finishdate, false);

            $binhireoptions = DB::table('tblbinserviceoptions')
                            ->leftJoin('tblbinservice', function ($query){
                                $query->on('tblbinservice.idBinType', '=', 'tblbinserviceoptions.idBinType')
                                	->on('tblbinservice.idSupplier', '=', 'tblbinserviceoptions.idSupplier');
                                })
                            ->select('tblbinserviceoptions.idBinServiceOptions', 'tblbinserviceoptions.extraHireagePrice', 'tblbinserviceoptions.extraHireageDays', 'tblbinserviceoptions.excessWeightPrice')
                            ->where([
								'tblbinserviceoptions.idBinType' => $binhire->idBinType,
								'tblbinserviceoptions.idSupplier' =>$invoiceDetails->idSupplier
                            ])
							->orderBy('tblbinserviceoptions.idBinServiceOptions', 'tblbinserviceoptions.extraHireagePrice', 'tblbinserviceoptions.extraHireageDays', 'tblbinserviceoptions.excessWeightPrice')
							->first();

			if ($datemargin > $binhireoptions->extraHireageDays){
				$daysmargin = $datemargin - $binhireoptions->extraHireageDays;
			} else {
				$daysmargin = 0;
			}

			return view('invoice_content',  ['invoiceDetails' => $invoiceDetails, 'binhire' => $binhire, 'customerdetails'=> $customerdetails,
				'supplierdetails' => $supplierdetails, 'binhireoptions' => $binhireoptions, 'daysmargin' => $daysmargin]);
        }
	}

	public function test_mail(Request $request){
		$idorderservice = $request['idorderservice'];
		$invoiceDetails = DB::table('tblorderservice')
        				->select('idSupplier','idBinService','idConsumer','paymentUniqueCode',
						'totalServiceCharge', 'subtotal','gst', 'bookingfee',
						'deliveryDate', 'collectionDate', 'deliveryAddress', 'deliveryComments', 'orderDate', 'card_category','card_type','card_number','card_holder')
        				->where(['idOrderService' => $idorderservice])
        				->first();
        if (is_null($invoiceDetails)){
        	return view('order_receipt', [ 'errorStatus' => 'invoice_not_found']);
        } else {
        	$binhire = DB::table('tblbinservice')
			->leftJoin('tblsize', 'tblsize.idSize', '=','tblbinservice.idBinSize')
			->leftJoin('tblbintype','tblbintype.idBinType','=','tblbinservice.idBinType')
			->leftJoin('tblbinnondelivery', 'tblbinservice.idBinType','=', 'tblbinnondelivery.idBinType')
			->leftJoin('tblbinserviceupdates','tblbinservice.idBinService','=','tblbinserviceupdates.idBinService')
			->leftJoin('tblbinserviceoptions', 'tblbinservice.idBinType','=', 'tblbinserviceoptions.idBinType')
			->leftJoin('tbluserservicearea', 'tblbinservice.idSupplier', '=', 'tbluserservicearea.idSupplier')
			->leftJoin('tblservicearea', 'tblservicearea.idArea','=','tbluserservicearea.idServiceArea')
			->select('tblbinservice.price', 'tblbinservice.stock', 'tblbinservice.idBinService', 'tblbinservice.idBinType',
                                        'tblbinservice.idBinSize','tblbinservice.idSupplier', 'tblbintype.name', 'tblbintype.description', 'tblbintype.description2','tblsize.size')
			->where(['tblbinservice.idBinService' => $invoiceDetails->idBinService])
			->groupBy('tblbinservice.price', 'tblbinservice.stock', 'tblbinservice.idBinService', 'tblbinservice.idBinType',
								'tblbinservice.idBinSize','tblbinservice.idSupplier', 'tblbintype.name', 'tblbintype.description','tblbintype.description2', 'tblsize.size')
            ->orderBy('tblbinservice.price','asc')
            ->first();

            $customerdetails = DB::table('tblcustomer')
        		->select('idCustomer', 'name', 'address', 'email', 'phone', 'suburb', 'zipcode')
        		->where(['idCustomer' => $invoiceDetails->idConsumer])
        		->first();

        	$supplierdetails = DB::table('tblsupplier')
            					->select('name', 'contactName', 'phonenumber', 'email', 'mobilePhone', 'fullAddress', 'email2',
                                                    'customerServiceContact', 'customerServicePhone', 'customerServiceMobile','abn')
            					->where(['idSupplier' => $invoiceDetails->idSupplier])
            					->first();

			$deliveryYear = date('Y', strtotime($invoiceDetails->deliveryDate));
			$deliveryMonth = date('m', strtotime($invoiceDetails->deliveryDate));
			$deliveryDay = date('d', strtotime($invoiceDetails->deliveryDate));

			$collectionYear = date('Y', strtotime($invoiceDetails->collectionDate));
			$collectionMonth = date('m', strtotime($invoiceDetails->collectionDate));
			$collectionDay = date('d', strtotime($invoiceDetails->collectionDate));

			$startdate = Carbon::createFromDate($deliveryYear, $deliveryMonth, $deliveryDay);
			$finishdate = Carbon::createFromDate($collectionYear, $collectionMonth, $collectionDay);
			$datemargin = $startdate->diffInDays($finishdate, false);

            $binhireoptions = DB::table('tblbinserviceoptions')
                            ->leftJoin('tblbinservice', function ($query){
                                $query->on('tblbinservice.idBinType', '=', 'tblbinserviceoptions.idBinType')
                                	->on('tblbinservice.idSupplier', '=', 'tblbinserviceoptions.idSupplier');
                                })
                            ->select('tblbinserviceoptions.idBinServiceOptions', 'tblbinserviceoptions.extraHireagePrice', 'tblbinserviceoptions.extraHireageDays', 'tblbinserviceoptions.excessWeightPrice')
                            ->where([
								'tblbinserviceoptions.idBinType' => $binhire->idBinType,
								'tblbinserviceoptions.idSupplier' =>$invoiceDetails->idSupplier
                            ])
							->orderBy('tblbinserviceoptions.idBinServiceOptions', 'tblbinserviceoptions.extraHireagePrice', 'tblbinserviceoptions.extraHireageDays', 'tblbinserviceoptions.excessWeightPrice')
							->first();

			if ($datemargin > $binhireoptions->extraHireageDays){
				$daysmargin = $datemargin - $binhireoptions->extraHireageDays;
			} else {
				$daysmargin = 0;
			}

			return view('mails.mail_invoice',  ['invoiceDetails' => $invoiceDetails, 'binhire' => $binhire, 'customerdetails'=> $customerdetails,
				'supplierdetails' => $supplierdetails, 'binhireoptions' => $binhireoptions, 'daysmargin' => $daysmargin]);
        }
	}

	public function pdfexporter(Request $request){
		$idorderservice = $request['idorderservice'];
		$invoiceDetails = DB::table('tblorderservice')
        				->select('idSupplier','idBinService','idConsumer','paymentUniqueCode',
						'totalServiceCharge', 'subtotal','gst', 'bookingfee',
						'deliveryDate', 'collectionDate', 'deliveryAddress', 'deliveryComments', 'orderDate')
        				->where(['idOrderService' => $idorderservice])
        				->first();

        if (is_null($invoiceDetails)){
        	return view('order_receipt', [ 'errorStatus' => 'invoice_not_found']);
        } else {
        	$binhire = DB::table('tblbinservice')
			->leftJoin('tblbintype','tblbintype.idBinType','=','tblbinservice.idBinType')
			->leftJoin('tblsize', 'tblsize.idSize', '=','tblbinservice.idBinSize')
			->leftJoin('tblbinnondelivery', 'tblbinservice.idBinType','=', 'tblbinnondelivery.idBinType')
			->leftJoin('tblbinserviceupdates','tblbinservice.idBinService','=','tblbinserviceupdates.idBinService')
			->leftJoin('tblbinserviceoptions', 'tblbinservice.idBinType','=', 'tblbinserviceoptions.idBinType')
			->leftJoin('tbluserservicearea', 'tblbinservice.idSupplier', '=', 'tbluserservicearea.idSupplier')
			->leftJoin('tblservicearea', 'tblservicearea.idArea','=','tbluserservicearea.idServiceArea')
			->select('tblbinservice.price', 'tblbinservice.stock', 'tblbinservice.idBinService', 'tblbinservice.idBinType',
                                        'tblbinservice.idBinSize','tblbinservice.idSupplier', 'tblbintype.name', 'tblbintype.description', 'tblbintype.description2','tblsize.size')
			->where(['tblbinservice.idBinService' => $invoiceDetails->idBinService])
			->groupBy('tblbinservice.price', 'tblbinservice.stock', 'tblbinservice.idBinService', 'tblbinservice.idBinType',
								'tblbinservice.idBinSize','tblbinservice.idSupplier', 'tblbintype.name', 'tblbintype.description', 'tblbintype.description2','tblsize.size')
            ->orderBy('tblbinservice.price','asc')
            ->first();

			if (is_null($binhire)){
				return view('order_receipt', [ 'errorStatus' => 'invoice_not_found']);
			}
            $customerdetails = DB::table('tblcustomer')
        		->select('idCustomer', 'name', 'address', 'email', 'phone', 'suburb', 'zipcode')
        		->where(['idCustomer' => $invoiceDetails->idConsumer])
        		->first();

        	$supplierdetails = DB::table('tblsupplier')
            					->select('name', 'contactName', 'phonenumber', 'email', 'mobilePhone', 'fullAddress', 'email2',
                                                    'customerServiceContact', 'customerServicePhone', 'customerServiceMobile','abn')
            					->where(['idSupplier' => $invoiceDetails->idSupplier])
            					->first();

            $binhireoptions = DB::table('tblbinserviceoptions')
                            ->leftJoin('tblbinservice', function ($query){
                                $query->on('tblbinservice.idBinType', '=', 'tblbinserviceoptions.idBinType')
                                	->on('tblbinservice.idSupplier', '=', 'tblbinserviceoptions.idSupplier');
                                })
                            ->select('tblbinserviceoptions.idBinServiceOptions', 'tblbinserviceoptions.extraHireagePrice', 'tblbinserviceoptions.extraHireageDays', 'tblbinserviceoptions.excessWeightPrice')
                            ->where([
								'tblbinserviceoptions.idBinType' => $binhire->idBinType,
								'tblbinserviceoptions.idSupplier' =>$invoiceDetails->idSupplier
                            ])
							->orderBy('tblbinserviceoptions.idBinServiceOptions', 'tblbinserviceoptions.extraHireagePrice', 'tblbinserviceoptions.extraHireageDays', 'tblbinserviceoptions.excessWeightPrice')
							->first();
			$bookingprice = $this->get_bookingprice();

			$deliveryYear = date('Y', strtotime($invoiceDetails->deliveryDate));
			$deliveryMonth = date('m', strtotime($invoiceDetails->deliveryDate));
			$deliveryDay = date('d', strtotime($invoiceDetails->deliveryDate));

			$collectionYear = date('Y', strtotime($invoiceDetails->collectionDate));
			$collectionMonth = date('m', strtotime($invoiceDetails->collectionDate));
			$collectionDay = date('d', strtotime($invoiceDetails->collectionDate));

			$startdate = Carbon::createFromDate($deliveryYear, $deliveryMonth, $deliveryDay);
			$finishdate = Carbon::createFromDate($collectionYear, $collectionMonth, $collectionDay);
			$datemargin = $startdate->diffInDays($finishdate, false);

			if ($datemargin > $binhireoptions->extraHireageDays){
				$daysmargin = $datemargin - $binhireoptions->extraHireageDays;
			} else {
				$daysmargin = 0;
			}

			// $supplier = tblsupplier::find($invoiceDetails->idSupplier);
			// $bookingFee = $supplier->bookingFee;
	        // $total_fee = ($bookingFee->type == 'fix') ? $bookingFee->amount : $invoiceDetails->subtotal * $bookingFee->amount/ 100;

			$data =  [
				'invoiceDetails' => $invoiceDetails,
				'binhire' => $binhire,
				'customerdetails'=> $customerdetails,
				'supplierdetails' => $supplierdetails,
				'binhireoptions' => $binhireoptions,
				'bookingprice' => $bookingprice,
				'daysmargin' => $daysmargin,
				// 'bookingFee' => $bookingFee,
			];

			return view('pdf.orderslip', $data);

			$pdf = PDF::loadView('pdf.orderslip', $data);

			$filename = 'Order Slip '.$invoiceDetails->paymentUniqueCode.'.pdf';
			return $pdf->download($filename);
		}
	}

	public function invoice_pdfexporter(Request $request){
		$idorderservice = $request['idorderservice'];
		$invoiceDetails = DB::table('tblorderservice')
        				->select('idSupplier','idBinService','idConsumer','paymentUniqueCode', 'totalServiceCharge', 'subtotal',
						'gst', 'bookingfee',
						'deliveryDate', 'collectionDate', 'deliveryAddress', 'deliveryComments', 'orderDate')
        				->where(['idOrderService' => $idorderservice])
        				->first();
        if (is_null($invoiceDetails)){
        	return view('order_receipt', [ 'errorStatus' => 'invoice_not_found']);
        } else {
        	$binhire = DB::table('tblbinservice')
			->leftJoin('tblbintype','tblbintype.idBinType','=','tblbinservice.idBinType')
			->leftJoin('tblsize', 'tblsize.idSize', '=','tblbinservice.idBinSize')
			->leftJoin('tblbinnondelivery', 'tblbinservice.idBinType','=', 'tblbinnondelivery.idBinType')
			->leftJoin('tblbinserviceupdates','tblbinservice.idBinService','=','tblbinserviceupdates.idBinService')
			->leftJoin('tblbinserviceoptions', 'tblbinservice.idBinType','=', 'tblbinserviceoptions.idBinType')
			->leftJoin('tbluserservicearea', 'tblbinservice.idSupplier', '=', 'tbluserservicearea.idSupplier')
			->leftJoin('tblservicearea', 'tblservicearea.idArea','=','tbluserservicearea.idServiceArea')
			->select('tblbinservice.price', 'tblbinservice.stock', 'tblbinservice.idBinService', 'tblbinservice.idBinType',
                                        'tblbinservice.idBinSize','tblbinservice.idSupplier', 'tblbintype.name', 'tblbintype.description', 'tblbintype.description2','tblsize.size')
			->where(['tblbinservice.idBinService' => $invoiceDetails->idBinService])
			->groupBy('tblbinservice.price', 'tblbinservice.stock', 'tblbinservice.idBinService', 'tblbinservice.idBinType',
								'tblbinservice.idBinSize','tblbinservice.idSupplier', 'tblbintype.name', 'tblbintype.description','tblbintype.description2', 'tblsize.size')
            ->orderBy('tblbinservice.price','asc')
            ->first();

            $customerdetails = DB::table('tblcustomer')
        		->select('idCustomer', 'name', 'address', 'email', 'phone', 'suburb', 'zipcode')
        		->where(['idCustomer' => $invoiceDetails->idConsumer])
        		->first();

        	$supplierdetails = DB::table('tblsupplier')
            					->select('name', 'contactName', 'phonenumber', 'email', 'mobilePhone', 'fullAddress', 'email2',
                                                    'customerServiceContact', 'customerServicePhone', 'customerServiceMobile','abn')
            					->where(['idSupplier' => $invoiceDetails->idSupplier])
            					->first();

            $binhireoptions = DB::table('tblbinserviceoptions')
                            ->leftJoin('tblbinservice', function ($query){
                                $query->on('tblbinservice.idBinType', '=', 'tblbinserviceoptions.idBinType')
                                	->on('tblbinservice.idSupplier', '=', 'tblbinserviceoptions.idSupplier');
                                })
                            ->select('tblbinserviceoptions.idBinServiceOptions', 'tblbinserviceoptions.extraHireagePrice', 'tblbinserviceoptions.extraHireageDays', 'tblbinserviceoptions.excessWeightPrice')
                            ->where([
								'tblbinserviceoptions.idBinType' => $binhire->idBinType,
								'tblbinserviceoptions.idSupplier' =>$invoiceDetails->idSupplier
                            ])
							->orderBy('tblbinserviceoptions.idBinServiceOptions', 'tblbinserviceoptions.extraHireagePrice', 'tblbinserviceoptions.extraHireageDays', 'tblbinserviceoptions.excessWeightPrice')
							->first();
			$bookingprice = $this->get_bookingprice();

			$deliveryYear = date('Y', strtotime($invoiceDetails->deliveryDate));
			$deliveryMonth = date('m', strtotime($invoiceDetails->deliveryDate));
			$deliveryDay = date('d', strtotime($invoiceDetails->deliveryDate));

			$collectionYear = date('Y', strtotime($invoiceDetails->collectionDate));
			$collectionMonth = date('m', strtotime($invoiceDetails->collectionDate));
			$collectionDay = date('d', strtotime($invoiceDetails->collectionDate));

			$startdate = Carbon::createFromDate($deliveryYear, $deliveryMonth, $deliveryDay);
			$finishdate = Carbon::createFromDate($collectionYear, $collectionMonth, $collectionDay);
			$datemargin = $startdate->diffInDays($finishdate, false);

			if ($datemargin > $binhireoptions->extraHireageDays){
					$daysmargin = $datemargin - $binhireoptions->extraHireageDays;
			} else {
				$daysmargin = 0;
			}

			$data =  ['invoiceDetails' => $invoiceDetails, 'binhire' => $binhire, 'customerdetails'=> $customerdetails,
					'supplierdetails' => $supplierdetails, 'binhireoptions' => $binhireoptions, 'bookingprice' => $bookingprice, 'daysmargin' => $daysmargin];
			$pdf = PDF::loadView('pdf.template', $data);

			$filename = 'Invoice '.$invoiceDetails->paymentUniqueCode.'.pdf';
			return $pdf->download($filename);
		}
	}

	public function update_order_status(Request $request){
		$idOrder = $request['idOrder'];
		$idSupplier = $request['idSupplier'];
		$status = $request['selectstatus'];

		$validate = Validator::make($request->all(), [
            'selectstatus' => 'required|integer',
        ], [
            'selectstatus.integer' => 'Select a status first !',
        ]);

		 if($validate->fails()){
         	$supplierdata = $this->get_supplier_data();
            return Redirect::route('supplier_supplies', ['supplierdata' =>$supplierdata])->withErrors($validate)->withInput();
        }


		$verify = DB::table('tblorderstatus')
				->leftJoin('tblorderservice', 'tblorderservice.idOrderService', '=', 'tblorderstatus.idOrder')
				->select('tblorderstatus.idOrderStatus', 'tblorderstatus.idOrder', 'tblorderstatus.operator', 'tblorderservice.idSupplier', 'tblorderservice.paymentUniqueCode')
				->where([
					'tblorderstatus.idOrder' => $idOrder,
					'tblorderservice.idSupplier' => $idSupplier
				])
				->first();

		if(!is_null($verify)){
			$update = $this->update_order_status_method($idOrder, $status);
			if($update['status'] == 'success'){
				$idSupplier = $verify->idSupplier;
				$orderref = $verify->paymentUniqueCode;
				$send = $this->order_status_mail_confirmation($idSupplier, $idOrder, $orderref, $status);
				if($send == 'success'){
					$request->session()->flash('status', 'success');
					$request->session()->flash('message', 'Success updating order data. An email has been sent to the customer');
					return redirect()->route('order.receipt',  ['idSupplier' => $idSupplier]);
				} else {
					$idSupplier = $verify->idSupplier;
					$request->session()->flash('status', 'danger');
					$request->session()->flash('message', 'Something is wrong on sending the confirmation email');
					return redirect()->route('order.receipt',  ['idSupplier' => $idSupplier]);
				}

			} else {
				$idSupplier = $verify->idSupplier;
				$request->session()->flash('status', 'danger');
				$request->session()->flash('message', 'Something is wrong on updating order data');
				return redirect()->route('order.receipt',  ['idSupplier' => $idSupplier]);
			}
		} else {
				$idSupplier = $request['idSupplier'];
        		$request->session()->flash('status', 'danger');
				$request->session()->flash('message', 'Something is wrong on updating order data');
				return redirect()->route('order.receipt',  ['idSupplier' => $idSupplier]);
		}
	}

	private function update_order_status_method($idOrder, $status){
		$orderstatus = new tblorderstatus();

		$updatedrow = $orderstatus::where('idOrder','=', $idOrder)->update([
			'status' => $status,
			'updated_at' => date('Y-m-d H:i:s',strtotime('now')),
			'operator' => session('idUser')
			]);
		if($updatedrow){
			$result['status'] = 'success';
		} else {
			$updatedrow['status'] = 'danger';
		}

		return $result;

	}

	private function order_status_mail_confirmation($idSupplier, $idOrder, $orderref,$status){
		$invoiceDetails = $this->get_order_detail($orderref, $idSupplier);

		if(!is_null($invoiceDetails)){
			$binhire = $this->get_bin_hire($invoiceDetails->idBinService);
			$customerdetails = $this->get_customer_detail($invoiceDetails->idConsumer);

		} else {
			$status = 'danger';
			return $status;
		}

		$supplierdetails = $this->get_supplier_data_by_id($idSupplier);

		if (!is_null($binhire)){
			$binhireoptions = $this->get_bin_service_options($idSupplier, $binhire->idBinType);
		} else {
			$status = 'danger';
			return $status;
		}

		$orderstatus = $this->order_status_description($status);

		$email = $customerdetails->email;
		$contactName = $customerdetails->name;

		$subject = "Your order status updates. Your order is now ".$orderstatus;
		Mail::send('mails.orderstatus_confirmation', ['invoiceDetails' => $invoiceDetails, 'binhire' => $binhire, 'customerdetails'=> $customerdetails,
				'supplierdetails' => $supplierdetails, 'binhireoptions' => $binhireoptions, 'orderstatus' => $orderstatus],
		function($mail) use ($email, $contactName, $subject){
			$mail->from("admin@ezyskipsonline.com.au", "Ezy Skips Online");
			$mail->to($email, $contactName);
			$mail->subject($subject);
		});
		$status = 'success';
		return $status;

	}

	private function order_status_description($status){
		if($status == 2){
			return 'Accepted';
		} elseif ($status == 3){
			return 'Delivered';
		} elseif ($status == 4){
			return 'Cancelled, Refund Issued';
		} elseif ($status == 5){
			return 'Cancelled, Refund Paid';
		}
	}

	private function get_bookingprice(){
		$bookingprice = DB::table('tblbookingprice')
					->select('price')
					->first();
		return $bookingprice;
	}
}
