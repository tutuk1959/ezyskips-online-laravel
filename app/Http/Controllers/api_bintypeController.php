<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\tblbintype;

class api_bintypeController extends ApiController
{
	public function showBinType(Request $request){
		if ($request->isMethod('get')){
			$size = tblsize::all();
            if (!$size) {
				return $this->sendError('Bin type not found');
            } else {
				$messages = 'Success!';
        		return $this->sendResponse($messages, $bintype);
            }
        } else {
            return $this->sendError('Bin type not found');
        }
        return $this->response->errorInternalError('Internal server error');
		}

        $bintype = tblbintype::paginate();
        if (!$bintype) {
          	return $this->sendError('Bin type not found');
        } else {
        	$messages = 'Success!';
        	return $this->sendResponse($messages, $bintype);
        }
    }

     public function findSize(Request $request){
        if ($request->isMethod('get')){
            $idSize = $request['idSize'];
            $size = tblsize::where(['idSize' => $idSize])->first();
            if (!$size) {
                return $this->response->errorNotFound('No bin size data');
            } else {
                return $this->response->withItem($size, new  sizeTransformer());
            }
        } else {
            return $this->response->errorInternalError('Internal server error');
        }
        return $this->response->errorInternalError('Internal server error');
    }

}
