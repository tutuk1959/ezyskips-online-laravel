<?php

namespace App\Http\Controllers;

use App\BookingFeeSetting;
use Illuminate\Http\Request;
use App\tblsupplier;

class BookingFeeSettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $suppliers = tblsupplier::with([
                'bookingFee',
                'user'
            ])
            ->approved()
            ->get();

        return view('booking-fee.index', compact('suppliers'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\BookingFeeSetting  $bookingFeeSetting
     * @return \Illuminate\Http\Response
     */
    public function edit(tblsupplier $tblsupplier)
    {
        $supplier = $tblsupplier;

        return view('booking-fee.edit', compact('supplier'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\BookingFeeSetting  $bookingFeeSetting
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, tblsupplier $tblsupplier)
    {
        $bookingFee = BookingFeeSetting::updateOrCreate([
            'supplier_id' => $tblsupplier->idSupplier
        ], [
            'type' => $request->type,
            'amount' => $request->amount,
        ]);

        return redirect()
            ->route('booking-fees.index')
            ->with([
                'status' => 'success',
                'message' => "Fee for {$tblsupplier->contactName} is set."
            ]);
    }
}
