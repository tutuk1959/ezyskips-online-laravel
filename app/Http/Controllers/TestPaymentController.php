<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Carbon\Carbon;

class TestPaymentController extends Controller
{
    public function __invoke()
    {
        $apiPaymentUrl = route('url-payment', [
            'idbinhire' => 142,
            'zipcode'   => 6111,
            'deliverydate'  => Carbon::now()->addDay()->format('Y-m-d'),
            'collectiondate' => Carbon::now()->addDays(3)->format('Y-m-d'),
        ]);

        // specific for tirta docker environment
        if (str_contains($apiPaymentUrl, 'localhost:18080'))
            $apiPaymentUrl = str_replace_first('localhost:18080', 'localhost:8080', $apiPaymentUrl);

        $client = new Client();

        $apiResponse = $client->get($apiPaymentUrl);

        $apiResponseBody = $apiResponse->json();

        $response = "<h3>Please visit the URL below to continue the payment.</h3>";
        $response .= "<p><a href='{$apiResponseBody['data']['payment_detail_url']}'>{$apiResponseBody['data']['payment_detail_url']}</a></p>";
        
        return $response;
        
        return 'OK';
    }
}
