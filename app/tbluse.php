<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class tbluse extends Model
{
	protected $table = 'tbluser';

	public $timestamps = false;

	protected $hidden = 'password';

	protected $casts = [
		'registerDate' => 'date'
	];
}
