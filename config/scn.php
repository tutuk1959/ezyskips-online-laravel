<?php
	/*
	| SCNet Payment Gateway
	| Documentation: https://docs.fatzebra.com/reference#hosted-payment-pages
	*/
	return [
		'base_url' => env('SCN_BASE_URL'),
		'username' => env('SCN_USERNAME'),
		'shared_secret' => env('SCN_SHARED_SECRET'),
		'currency' => env('SCN_CURRENCY'),
		'token' => env('SCN_TOKEN'),
	];
