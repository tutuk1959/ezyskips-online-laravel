$(document).ready(function(){
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});
	$('div.alert-success').css('display', 'none');
	$('div.alert-danger').css('display', 'none');
	$('div.loading').css('display', 'block');
	var data = $('#verification').val();
	var vi = $('#v').val();
	var reference = $('#reference').val();
	var card_category = $('#card_category').val();
	var card_type = $('#card_type').val();
	var card_holder = $('#card_holder').val();
	var card_number = $('#card_number').val();
	$.ajax({
		dataType: 'json',
		type:'POST',
		url: url,
		data:{data:data, v:vi, reference:reference, card_category:card_category, card_type:card_type ,card_holder:card_holder, card_number:card_number }
	}).done(function(data){
		console.log(data);
		if(data == 1){
			$('div.alert-success').css('display', 'block');
			$('div.loading').css('display', 'none');
			dataLayer.push({
				'event': 'purchase',
				'transactionId': trx.id,
				'transactionTotal': trx.total,
				'transactionTax': trx.gst,
				'transactionProducts': [{
					'sku': trx.sku,
					'name': trx.name,
					'price': trx.price,
					'quantity': 1
				}]
			});
		} else if(data == 0) {
			$('div.alert-danger').css('display', 'block');
			$('div.loading').css('display', 'none');
		}
    }).fail(function() {
		console.log('Error');
  });
});